# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kfilemetadata
pkgver=5.91.0
pkgrel=0
pkgdesc="A library for extracting file metadata"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends_dev="qt5-qtbase-dev karchive-dev kcoreaddons-dev ki18n-dev kconfig-dev exiv2-dev taglib-dev ffmpeg-dev attr-dev"
makedepends="$depends_dev extra-cmake-modules qt5-qttools-dev doxygen"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kfilemetadata-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build

	# Broken tests
	local skipped_tests="("
	local tests="
		usermetadatawritertest
		extractorcoveragetest
		propertyinfotest_localized
		extractorcollectiontest
		"
	for test in $tests; do
		skipped_tests="$skipped_tests|$test"
	done
	skipped_tests="$skipped_tests)test"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "$skipped_tests"
}


package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
73a8c730f349bffd2db1aea5ffea65296d871f9fe7f7b4e506e7adf8b0573fd19fc9dc36decc52f0f78538d07f2fe2520c1bf87be16908f98be4bd68ecf7b9d5  kfilemetadata-5.91.0.tar.xz
"
