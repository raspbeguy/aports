# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kscreen
pkgver=5.24.2
pkgrel=0
pkgdesc="KDE's screen management software"
# armhf blocked by qt5-qtdeclarative
# s390x and riscv64 blocked by polkit -> kconfigwidgets
arch="all !armhf !s390x !riscv64"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
depends="hicolor-icon-theme"
makedepends="
	extra-cmake-modules
	kcmutils-dev
	kconfig-dev
	kconfigwidgets-dev
	kdbusaddons-dev
	kdeclarative-dev
	kglobalaccel-dev
	ki18n-dev
	kiconthemes-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libkscreen-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtsensors-dev
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kscreen-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# kscreen-kded-configtest is broken
	# kscreen-kded-osdtest hangs
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "kscreen-kded-(config|osd)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
9e760b231086aff40bfa426fbbed7301759d59b671252d8e253017e3a75889cbb48dd8f3f8a03266a63b921d9ae3fea7dbefa5effa14b3e899b6b1ba050aa969  kscreen-5.24.2.tar.xz
"
