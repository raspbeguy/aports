# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kpeople
pkgver=5.91.0
pkgrel=0
pkgdesc="A library that provides access to all contacts and the people who hold them"
arch="all !armhf" # armhf blocked by qt5-qtdeclarative
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends="qt5-qtbase-sqlite"
depends_dev="qt5-qtdeclarative-dev kcoreaddons-dev kwidgetsaddons-dev ki18n-dev kitemviews-dev kservice-dev"
makedepends="$depends_dev extra-cmake-modules qt5-qttools-dev doxygen"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kpeople-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	# personsmodeltest fails
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E '(personsmodeltest)'
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}
sha512sums="
6295cb0e390ffce8771686a297c1ffc7afaa5bb03fe1f6cadb8997d48e4c88faad2cd92dd009ad1a07cddc178fefa8c6d35bc5b04c3ab33a46a1710e43bd28d6  kpeople-5.91.0.tar.xz
"
