# Contributor: Sören Tempel <soeren+alpinelinux@soeren-tempel.net>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=harfbuzz
pkgver=4.0.0
pkgrel=0
pkgdesc="Text shaping library"
url="https://freedesktop.org/wiki/Software/HarfBuzz"
arch="all"
license="MIT"
makedepends="freetype-dev glib-dev gobject-introspection-dev cairo-dev icu-dev
	graphite2-dev meson gtk-doc"
checkdepends="python3"
subpackages="$pkgname-static $pkgname-dev $pkgname-icu $pkgname-utils $pkgname-doc"
source="harfbuzz-$pkgver.tar.gz::https://github.com/harfbuzz/harfbuzz/archive/$pkgver.tar.gz"

build() {
	abuild-meson \
		--default-library=both \
		-Dglib=enabled \
		-Dgobject=enabled \
		-Dgraphite=enabled \
		-Dicu=enabled \
		-Dfreetype=enabled \
		-Dcairo=enabled \
		-Ddocs=enabled \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild -v -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

static() {
	pkgdesc="$pkgdesc (static libraries)"

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/*.a "$subpkgdir"/usr/lib/
}

icu() {
	pkgdesc="Harfbuzz ICU support library"
	replaces="harfbuzz"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/lib*icu.so.* "$subpkgdir"/usr/lib/
}

utils() {
	pkgdesc="$pkgdesc (utilities)"
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

sha512sums="
d2b740f3392cd2763f467298dcd384e74fdbf592ebc066125451f886163c24bfe7993eb43f34d0ea3933ad16eee1c6f6ebe4e95f48cc13ac6650c64dd0b4ba8c  harfbuzz-4.0.0.tar.gz
"
