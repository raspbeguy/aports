# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=kopia
pkgver=0.10.5
pkgrel=0
pkgdesc="Fast and secure backup tool"
url="https://kopia.io/"
license="Apache-2.0"
arch="all !armhf !armv7 !x86" # tests fail with out of memory error
makedepends="go"
subpackages="$pkgname-bash-completion $pkgname-zsh-completion"
checkdepends="openssh-keygen"
source="https://github.com/kopia/kopia/archive/v$pkgver/kopia-$pkgver.tar.gz
	skip-docker-tests.patch
	"

export GOFLAGS="$GOFLAGS -trimpath -mod=readonly -modcacherw"
export GOPATH="$srcdir"
export CGO_ENABLED=0

build() {
	go build -ldflags "-s -w -X github.com/kopia/kopia/repo.BuildVersion=$pkgver"

	./kopia --completion-script-bash > $pkgname.bash
	./kopia --completion-script-zsh > $pkgname.zsh
}

check() {
	go test -tags testing ./...
}

package() {
	install -Dm755 kopia "$pkgdir"/usr/bin/kopia

	install -Dm644 $pkgname.bash "$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 $pkgname.zsh "$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
59060d09b241d7b4ef8847ce4359be04def1371b2d708c1404b317e971425892c7afeaa56f13b086fca4003d4d92a193e6b8b726df90922a15f4a2cdb6a06eab  kopia-0.10.5.tar.gz
6c1c8ca52d83c940c561f11adc18298147882b709810edd8c6560c8988ff1bd30dae2adba4c18055d283e7c2c655a6c6f10c3951829826d6fe5eea20c8cb821d  skip-docker-tests.patch
"
